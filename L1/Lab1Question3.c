#include <stdlib.h>
#include <stdio.h>

void swap(int* a, int i, int j)
{
  int temp = *(a+i);
  *(a + i) = *(a+j);
  *(a + j) = temp;
  return 0;
}

void mySort(int* a, int size) {
    int i,j;
    for(i=0;i<size - 1; ++i){
      for(j=0; j<size-1; ++j){
        if(*(a+j)>*(a+j+1)){
          swap(a,j,j+1);
        }
      }
    }
    return 0;
}

int main() {
  
  int* nums = (int*) malloc(5 * sizeof(int));
  nums[0]=5;
  nums[1]=12;
  nums[2]=3;
  nums[3]=3;
  nums[4]=-1;
  
  mySort(nums, 5);
  for(int i = 0; i<5;i++)
  {
    printf("%d\n",nums[i]);
  }
  free(nums);
  return 0;
}



