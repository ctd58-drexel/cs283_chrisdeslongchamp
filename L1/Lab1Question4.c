#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#define NULL 0

struct ListNode {
  int data;
  struct ListNode* next;
};

void swap(struct ListNode* prev,struct ListNode* current, struct ListNode* nxt){
  struct ListNode* temp;
  if(prev!=NULL){
    temp = nxt->next;
    prev->next = nxt;
    nxt->next = current;
    current->next=temp;
  }
  else{
    temp = nxt->next;
    nxt->next = current;
    current->next = temp;
  }
} 

struct ListNode* sortList(struct ListNode* root, int size){
  struct ListNode* tmpPtr = root;
  struct ListNode* tmpNxt = root->next;
  struct ListNode* tmpPrev = NULL;
  struct ListNode* tmp = NULL;
  int first = 0;
  struct ListNode* tmpFirst = root;
  if(size==1){
    return tmpFirst;
  }
  else{
    while(tmpNxt!=NULL){
        if(tmpPtr->data > tmpNxt->data){
          if(first==0){
            tmpFirst = tmpNxt;
            first = 1;
          }
          swap(tmpPrev, tmpPtr, tmpNxt);
          tmp = tmpPtr;
          tmpPtr = tmpNxt;
          tmpNxt = tmp;
        }
      
      tmpPrev = tmpPtr;
      tmpPtr = tmpPtr->next;
      tmpNxt = tmpNxt->next;
      first = 1;
    }
    return sortList(tmpFirst,size-1);
  }
}

void freeNodes(struct ListNode* root){
  if(root->next == NULL){
    free(root);
    return 0;
  }
  else{
    freeNodes(root->next);
  }
  free(root);
  return 0;
}

int main() {
  struct ListNode* node1 = (struct ListNode*) malloc(sizeof(struct ListNode));
  struct ListNode* node2 = (struct ListNode*) malloc(sizeof(struct ListNode));
  struct ListNode* node3 = (struct ListNode*) malloc(sizeof(struct ListNode));  
  int sum;
  
  node1->data = 4;
  node1->next = node2;
  
  node2->data = 2;
  node2->next = node3;
  
  node3->data = -1;
  node3->next = NULL;
  
  node1 = sortList(node1,3);
  node2 = node1->next;
  node3 = node2->next;
  
  freeNodes(node1);
  
  return 0;
}
