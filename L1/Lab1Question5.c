#include <stdlib.h>
#include <stdio.h>
#include <time.h>


int array_get(int* a, int pos);
void array_remove(int* a, int pos, int size);
int* array_add(int* a, int val, int* size);

int main() {
  clock_t start,end;
  double cpu_time_used;  
  int n, test_get;
  int* test_array;
  printf("Enter size of array\n");
  scanf("%d",&n);
  int* updated_size = (int*) malloc(sizeof(int));
  *updated_size = n;
  
  
  test_array = (int*) malloc(n * sizeof(int));
  
  for(int i = 0; i < n; i++) {
    
    *(test_array + i) = NULL;
  }
  
  *test_array = 1;
  *(test_array + 1) = 2;
  
  test_get = array_get(test_array, 0);
  array_remove(test_array, 0, 5);  
  start = clock();
  for(int j = 0; j<20000; j++){
	test_array = array_add(test_array, 7, updated_size);
  }
  end = clock();
  cpu_time_used = ((double)(end-start))/CLOCKS_PER_SEC;
  printf("%d\n",cpu_time_used);
  free(test_array);
  free(updated_size);
  return 0;
}

int array_get(int* a, int pos) {
  int this_get;
  
  this_get = *(a + pos);
  
  return this_get;
}

void array_remove(int* a, int pos, int size) {
  
  for(int i = pos; i < size; i++) {
    if(i+1 < size) {
      *(a + i) = *(a + i+1);
    }
    else {
      *(a + i) = NULL;
    }
  }
}

int* array_add(int* a, int val, int* size) {
  if(*(a + *size-1) != NULL) {
    a = realloc(a,(*size * sizeof(int)+sizeof(int)));
    
    *(a + *size) = val;
    
    *size = *size+1;
  }
  else {
    for(int i = 0; i < *size; i++) {
      if(*(a+i) == NULL) {
        *(a+i) = val;
        break;
      }
    }
  }
  return a;
}
