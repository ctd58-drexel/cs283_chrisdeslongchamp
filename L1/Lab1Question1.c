#include <stdio.h>
#include <stdlib.h>

int main() {
  
  int* my_int_p;
  
  int* malloc_ints = (int*) malloc(10 * sizeof(int));
  
  int* addr;
  int i;
  int j;
  
  for(i =0; i<10;i++){
    addr=( malloc_ints + i);
    *addr = i;
  }
  
  for(i=0; i<10; i++){
    addr = (malloc_ints + i);
    printf("%d\n",*addr);
  }
  
  free(malloc_ints);
  
  return 0;
}
