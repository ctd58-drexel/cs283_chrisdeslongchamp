#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main() {
  int i;
  int j;
  
  char** strings = (char**) malloc(10 * sizeof(char*));
  for(i=0; i<10; i++){
    strings[i] = (char*) malloc(15 * sizeof(char));
    strcpy(strings[i],"Hello noob boy");
  }
  
  for(i=0; i<10; i++) {
      printf("%s\n", strings[i]);
  }
  
  for(i=0; i<10; i++) {
      free(strings[i]);
  }
  free(strings);
  
  return 0;
}
