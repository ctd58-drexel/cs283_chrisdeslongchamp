#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

//returns the full file path for a file or directory
char* filePath(char* dbName, char* filename){
	char* file =(char*)(malloc((strlen(dbName)+strlen(filename)+5)*sizeof(char)));
	strcpy(file, dbName);
	strcat(file, "/");
	strcat(file, filename);
	strcat(file,".txt");
	printf("%s\n",file);
	return file;

}


void do_select(char* dbName)
{
	char* tablename;
	tablename = strtok(NULL, " ");
	tablename = strtok(NULL, " ");
	tablename = strtok(NULL, " ");
	char* fileLocation = filePath(dbName, tablename);
	char* value;
	char* field;
	int* fieldNum;
	field = strtok(NULL,"d");
	field=strtok(NULL,"=");
	value=strtok(NULL,"\"");
	fieldNum=atoi(field);
	char* tokens;
	char* lines=(char*)(malloc(500*sizeof(char)));
	FILE *t= fopen(fileLocation, "r");
	char* line=(char*)(malloc(500*sizeof(char)));
	//reads file till the end. If it finds a value that matches the requested
	//value and is in the requested column, it prints the row to the console.
	while(fgets(line,500,t)!=NULL){
		strcpy(lines,line);
		tokens = strtok(line,",");
		for(int i = 1; i<fieldNum;i++)
		{
			tokens = strtok(NULL,",");
		}
		if(strcmp(tokens,value)==0){
			printf("%s\n",lines);
		}
	}

}

void do_drop(char* dbName){
	char* tablename;
	tablename=strtok(NULL, " ");
	tablename=strtok(NULL, " ");
	char* fileLocation = filePath(dbName, tablename);
	
	//removes requested file
	remove(fileLocation);
}

void do_insert(char* dbName){
	char* tablename;
	tablename=strtok(NULL, " ");
	tablename=strtok(NULL, " ");
	char* fileLocation = filePath(dbName, tablename);
	FILE *file = fopen(fileLocation, "ab+");
	
	char* values;
	char* fileString;
	char* value;
  	values=strtok(NULL, ")");
	fileString = (char*) malloc((strlen(values)+1)*sizeof(char));
	printf("%s\n",values);
	value = strtok(values, "\"");
	value = strtok(NULL,"\"");
	printf("%s\n",value);
	
	//builds a string of the new row. once it is done building it prints
	//it to the end of the file.
	while(value!=NULL){
		if(fileString==NULL){
			strcpy(fileString,value);
			strcat(fileString,",");
		}
		else{
			strcat(fileString,value);
			strcat(fileString,",");
		}
		
		value = strtok(NULL, "\"");
		if(value!=NULL){
			value=strtok(NULL,"\"");
		}
		printf("%s\n",fileString);
	}
	printf("%s\n",fileString);
	fprintf(file, "%s\n", fileString);
	fclose(file);
}

void do_create(char* dbName){
	char* tablename;
	tablename=strtok(NULL, " ");
	tablename=strtok(NULL, " ");
	char* fileLocation = filePath(dbName, tablename);
	//creates a file in the desired location. Opens it for appending and reading
	FILE *file = fopen(fileLocation, "ab+");
	
	char* values;
	char* fileString;
	char* value;
  	values=strtok(NULL, "[");
	values=strtok(NULL,"]");
	fileString = (char*) malloc((strlen(values)+1)*sizeof(char));
	value = strtok(values, ",");
	//creates a string of the new table. Once it is done, it prints it to the file.
	while(value!=NULL){
		if(fileString==NULL){
			strcpy(fileString,value);
			strcat(fileString,",");
		}
		else{
			strcat(fileString,value);
			strcat(fileString,",");
		}
		value = strtok(NULL, ",");
	}
	
	fprintf(file, "%s\n", fileString);
	fclose(file);
}

void do_delete(char* dbName){
	char* tablename;
	tablename = strtok(NULL, " ");
	tablename = strtok(NULL, " ");
	char* fileLocation = filePath(dbName, tablename);
	char* value;
	char* field;
	int* fieldNum;
	field = strtok(NULL,"d");
	field=strtok(NULL,"=");
	value=strtok(NULL,"\"");
	fieldNum=atoi(field);
	char* tokens;
	char** fileLines = (char**)(malloc(10*sizeof(char*)));
	int j=0;
	char* lines=(char*)(malloc(500*sizeof(char)));
	FILE *t= fopen(fileLocation, "r");
	FILE *file=fopen("./replica.txt","ab+");
	char* line=(char*)(malloc(500*sizeof(char)));
	//prints only the lines not being deleted to the replica file
	while(fgets(line,500,t)!=NULL){
		strcpy(lines,line);
		tokens = strtok(line,",");
		for(int i = 1; i<fieldNum;i++)
		{
			tokens = strtok(NULL,",");
		}
		if(strcmp(tokens,value)!=0){
			fprintf(file,"%s",lines);
		}
	}
	//destroys the original file then renames the replica to the location and
	//name of the original
	remove(fileLocation);
	rename("./replica.txt",fileLocation);
	fclose(file);
}

void do_update(char* dbName)
{
	char* tablename;
	tablename = strtok(NULL, " ");
	char* fileLocation = filePath(dbName, tablename);
	char* newVal;
	char* changeField;
	int* changeFieldNum;
	changeField = strtok(NULL,"d");
	changeField=strtok(NULL,"=");
	newVal = strtok(NULL,"\"");
	changeFieldNum=atoi(changeField);
	char* value;
	char* field;
	int* fieldNum;
	field = strtok(NULL,"d");
	field=strtok(NULL,"=");
	value=strtok(NULL,"\"");
	fieldNum=atoi(field);
	char* tokens;
	char* tok;
	int j=1;
	char* fileLines = (char*)(malloc(500*sizeof(char)));
	char* lines=(char*)(malloc(500*sizeof(char)));
	FILE *t= fopen(fileLocation, "r");
	FILE *file=fopen("./replica","ab+");
	char* line=(char*)(malloc(500*sizeof(char)));
	char* changedLine = (char*)(malloc(500*sizeof(char)));
	//prints lines that do not need to be updated to the replica file
	//builds updated lines and prints it to the replica file
	while(fgets(line,500,t)!=NULL){
		strcpy(lines,line);
		tokens = strtok(line,",");
		for(int i = 1; i<fieldNum;i++)
		{
			tokens = strtok(NULL,",");
		}
		if(strcmp(tokens,value)==0){
			strcpy(fileLines,lines);
			printf("%s\n",fileLines);
			tok=strtok(fileLines,",");
			printf("%s\n",fileLines);
			while(tok!=NULL){
				if(j==changeFieldNum){
					if(changedLine==NULL){
						strcpy(changedLine,newVal);
						strcat(changedLine,",");
					}
					else{
						strcat(changedLine,newVal);
						strcat(changedLine,",");
					}
				}
				else{
					if(changedLine==NULL){
						strcpy(changedLine,tok);
						strcat(changedLine,",");
					}
					else{
						strcat(changedLine,tok);
						strcat(changedLine,",");
					}
				}
				j= j+1;
				tok=strtok(NULL,",");
			}
			fprintf(file,"%s\n",changedLine);
		}
		else{
			fprintf(file,"%s",lines);
		}
	}
	
	//removes the oringal file and relocates and renames the replica file
	//to the original
	remove(fileLocation);
	rename("./replica",fileLocation);
	fclose(file);
}
char* strip(char* x){
	//strips the \n off the line
	x[strlen(x)-1]='\0';
	return x;
}

int main() {
	char* dbName;
	char input[50];
	char* action;
	char* input2;
	printf("Enter the name of the database you wish to access.\n");
	fgets(input, 50, stdin);
	printf("Searching for %s\n", input);
	dbName = (char*) malloc((strlen(input) + 3) * sizeof(char));
	input2=input;
	input2=strip(input2);
	strcpy(dbName,"./");
	strcat(dbName, input2);
	mkdir(dbName,0777);
	printf("What would you like to do?\n");
	fgets(input, 500, stdin);
	action = (char*)malloc((strlen(input))*sizeof(char));
	strcpy(action, input);
	action = strip(action);
	char* firstword = strtok(action, " ");
	if(strcmp(firstword, "SELECT")==0){
		do_select(dbName);
	}
	else if(strcmp(firstword, "INSERT")==0){
		
		do_insert(dbName);
	}
	else if(strcmp(firstword, "DELETE")==0){
		do_delete(dbName);
	}
	else if(strcmp(firstword, "UPDATE")==0){
		do_update(dbName);
	}
	else if(strcmp(firstword, "CREATE")==0){
		do_create(dbName);
	}
	else if(strcmp(firstword, "DROP")==0){
		do_drop(dbName);
	}	
	printf("Done\n");
}
