To make the Assignment.o file just type make.
Clean will remove the Assignment.o

Example of a table:

table1,table2,table3
value1,value2,value3
pizza,pie,money

Make sure the name the file TableName.txt (if you are adding files to folders)
where TableName is the name of your table. If you are using the create call, you
do not need to add the .txt

Examples of calls

What database would you like to use?
test
Searching for test
What action would you like to take?
SELECT  * FROM table2 WHERE field1="pizza"
DELETE FROM table2 WHERE field2="Field2"
UPDATE table1 SET field1="pizza" WHERE field3="test3"
INSERT INTO table1 (field1="newvalue",field2="newervalue",field3="notValue",field4="pizza")
CREATE TABLE superTable FIELDS [field2,field3,field1,field10]
DROP TABLE superTable